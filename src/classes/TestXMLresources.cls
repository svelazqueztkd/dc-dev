/**
 * @description       : 
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 09-01-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-20-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
private with sharing class TestXMLresources {
  @TestSetup
  static void makeData(){
	  
      List<Manifest__c> manifiestoList=SP_XMLDataFactory.createManifestList();
      insert manifiestoList;
  } 
    
  
  
  @isTest
    public static void formaNombreXMLTest()
    {
      	Test.startTest();
		//obtengo fecha actual
        Date todaysDate = system.today();
        String strDay = (todaysDate.day() > 9 ) ? String.valueof(todaysDate.day()) : '0'+ String.valueof(todaysDate.day());
        String strMonth = (todaysDate.month() > 9) ? String.valueof(todaysDate.month()) : '0'+ String.valueof(todaysDate.month());
        String fecha=String.valueof(todaysDate.year())+ strMonth + strDay;

						
		List<Manifest__c> manifestList = [SELECT Id, Name FROM Manifest__c WHERE Type__c = 'Pasaporte 1ra Vez'];
		String manifiestoId=manifestList[0].Id;               
		String nameManifiesto=manifestList[0].Name;              
		//concateno
		if(nameManifiesto.indexOfIgnoreCase('M') == 0){
            nameManifiesto = nameManifiesto.removeStartIgnoreCase('M');
            nameManifiesto = 'T('+ nameManifiesto + ')';
        } 
		String fileName=nameManifiesto + fecha; 
		
		//creo la lista k contiene el Id de un manifiesto             
		List<String> manifiestos=new List<String>{manifiestoId};
		//manifiestos.add(manifiestoId);       

		String resultado=XMLresources.formaNombreXML(manifiestos);
		System.assertEquals(fileName, resultado, '');
		// System.assertEquals(fileName,resultado,'File name not expected');
		Test.stopTest();
	}

	@isTest
	static void procedureNumberTest(){
		Test.startTest();
		String result = XMLresources.procedureNumber('TC20002336');
		System.assertEquals('20002336', result, '');
		Test.stopTest();
	}

     @isTest
    public static void formateaFechaTest()  //String formateaFecha(Datetime fecha)
    {      
	  Test.startTest();
	  Date dt = Date.newInstance(2020, 8, 1);
      String fechaFormateada=XMLresources.formateaFecha(dt);
      System.assertEquals('2020-08-01T00:00:00.00-04:00',fechaFormateada, '');  
      Test.stopTest();  
    }

    @isTest
    public static void addFileXMLTest(){    
	  Test.startTest();
	  Manifest__c manifest = [SELECT Id, Name FROM Manifest__c WHERE Type__c = 'Pasaporte 1ra Vez' LIMIT 1];
      List<String> manifiestos=new List<String>();
      manifiestos.add(manifest.Id);    
	  String xmlString='<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'
	  					+ '<Tramites>'
						+ ' <Pasaporte1vez-2444>'
						+ '  <vch_nombre1>ELIZABETH</vch_nombre1>'
						+ '  <vch_nombre2>SIBERIA</vch_nombre2>'
						+ '  <vch_apellido1>BETANCOURT</vch_apellido1>'
						+ '  <vch_apellido2>HERRERA</vch_apellido2>'
						+ ' </Pasaporte1vez-2444>'
						+ '</Tramites>';    

      XMLresources.addFileXML(manifiestos, xmlString);
      List<StaticResource> docs = [select id, name, body from StaticResource];     
	  //System.assertEquals('T00252020720',docs[0].name,'The file not exist');
	  System.assert(docs.size() > 0, '');
      Test.stopTest();
	}
	
	@isTest
	static void mapMunicipioExisteTest(){
		Test.startTest();
		Integer intMunicipio = XMLresources.mapMunicipio('Cerro', 'La Habana');
		System.assert(intMunicipio != -1, '');
		Test.stopTest();
	}

	@isTest
	static void mapMunicipioNoExisteTest(){
		Test.startTest();
		Integer intMunicipio = XMLresources.mapMunicipio('Vedado', 'La Habana');
		System.assert(intMunicipio == -1, '');
		Test.stopTest();
	}

	@isTest
	static void mapProvinciaExisteTest(){
		Test.startTest();
		Integer intProvincia = XMLresources.mapProvincia('La Habana');
		System.assert(intProvincia != -1, '');
		Test.stopTest();
	}

	@isTest
	static void mapProvinciaNoExisteTest(){
		Test.startTest();
		Integer intProvincia = XMLresources.mapProvincia('Florida');
		System.assert(intProvincia == -1, '');
		Test.stopTest();
	}

	@isTest
	static void mapPaisExisteTest(){
		Test.startTest();
		Integer intPais = XMLresources.mapPais('US');
		System.assert(intPais != -1, '');
		Test.stopTest();
	}

	@isTest
	static void mapPaisNoExisteTest(){
		Test.startTest();
		Integer intPais = XMLresources.mapPais('Estocolmo');
		System.assert(intPais == -1, '');
		Test.stopTest();
	}
}