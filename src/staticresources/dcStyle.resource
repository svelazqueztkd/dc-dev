	/*** GLOBAL ***/
		html {
			background-color: #f5f8fb;
		}

		body {
			font-family: "Roboto",sans-serif;
			color: #334147;
		}

	/*** COLOR ***/
		.sp-color-brand {
			color: #F46B41;
		}

	/*** BCK ***/
		.sp-bck-lightgray {
			background-color: #f3f1f1;
		}

		.sp-bck-gray {
			background-color: #A5AEB2;
		}

	/*** TEXT ***/
		.sp-text-bold {
			font-weight: 500;
		}

	/*** HEADING ***/
        .sp-page-heading {
            padding-bottom: 1rem;
        }

	/*** BUTTONS ***/
        .sp-button-close {
            font-size: 1.475rem;
        }

	/*** SIDEBAR ***/
		.sp-sidebar-help {
			padding-left: .75rem;
			padding-right: .75rem;
			margin-top: 121px;
		}

		.sp-sidebar-help h2 {
			color: #A5AEB2;
			font-weight: 500;
			letter-spacing: .025rem;
		}

	/*** FORMS ***/
		.slds-input {
			border-radius: .15rem;
			min-height: calc(1.875rem + (6px * 2));
		}

		/*specific customization*/
        #anos-vividos-ultima-direcc {
            font-size: .875rem;
        }

		/*read ony style*/
		.sp-form-readonly .sp-field input, 
		.sp-form-readonly .sp-field select, 
		.sp-form-readonly .sp-field textarea {
			color: #9f9f9f;
			border-left: none;
			border-right: none;
			border-top: none;
			border-bottom: 1px solid #CCCCCC;
			background-color: transparent;
		}

			.sp-form-readonly .sp-field input:focus, 
			.sp-form-readonly .sp-field select:focus, 
			.sp-form-readonly .sp-field textarea:focus {
				box-shadow: none;
			}

 		/*remove select arrow*/
 		.slds-select_container:before {
			border-bottom: none;
		}
 	
 		.slds-select_container:after {
			border-top: none;
		}

		/*label transition*/
		.sp-field {
			display: flex;
			flex-flow: column-reverse;
		}

		.sp-field label, 
		.sp-field input,
		.sp-field textarea {
			transition: all 0.2s;
			touch-action: manipulation;
		}

		.sp-field label {
			font-size: 10px;
		}

		.sp-field input,
		.sp-field select,
		.sp-field textarea {
			color: #F46B41;
			font-size: 1.15em;
			padding-top: 10px;
			border: 1px solid #ccc;
			font-family: inherit;
			-webkit-appearance: none;
			border-radius: .15rem;
			padding: 0.35rem 0 0 .7rem;
			cursor: text;
			min-height: calc(1.875rem + (7px * 2));
		}

			.sp-field select {
				padding: 0 0 0 .7rem;
			}

			.sp-field textarea {
				padding: 2rem 0 0 .7rem;
				min-height: 6rem;
			}

			.sp-field.sp-field-textarea {
				line-height: initial; /* avoid error with textarea */
				top: -.725rem;
			}

		.sp-field input:focus,
		.sp-field textarea:focus {
			outline: 0;
		}

		.sp-field input:placeholder-shown + label,
		.sp-field textarea:placeholder-shown + label {
			cursor: text;
			color: #adadad;
			max-width: 66.66%;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
			transform-origin: left bottom;
			transform: translate(0, 2.125rem) scale(1.5);
			padding-left: .45rem ;
		}

		::-webkit-input-placeholder {
			opacity: 0;
			transition: inherit;
		}

		.sp-field input:focus::-webkit-input-placeholder,
		.sp-field textarea:focus::-webkit-input-placeholder {
			opacity: 1;
		}

		.sp-field input:not(:placeholder-shown) + label,
		.sp-field input:focus + label,
		.sp-field textarea:not(:placeholder-shown) + label,
		.sp-field textarea:focus + label {
			transform: translate(0, 0) scale(1);
			cursor: pointer;
			top: 18px;
			position: relative;
			padding-left: .7rem;
		}

		/*textarea*/
		.sp-field textarea:not(:placeholder-shown) + label,
		.sp-field textarea:focus + label {
			top: 28px;
		}

		.sp-field textarea:placeholder-shown + label {
			white-space: normal!important;
			top: .75rem;
			position: relative;
		}

			.sp-field textarea:placeholder-shown:focus + label {
				top: 1.75rem;
			}

			#textarea-id-01 {
				margin-top: .75rem;
			}

		/*select*/
		.sp-field.slds-select_container {
			padding-top: 1rem;
		}

		/* input icon */
		.sp-input-icon {
			position: absolute;
			right: .75rem;
			top: 2rem;
		}

		.sp-button-brand {
			background: #f46b41;
			color: white;
			padding: .35rem 1.25rem;
			font-size: 1rem;
		}

		.sp-button-brand:hover,
		.sp-button-brand:focus {
			background: #d55d38;
			color: white;
		}
		
		.sp-button-sm {
			background: #f46b41;
			color: white;
			padding: 0rem 0.85rem;
			font-size: .75rem;
		}
		
		/* error message */
        .sp-has-error input,
		.sp-has-error textarea,
		.sp-has-error select {
            border-color: #c23934;
    		color: #c23934;
    		background: rgb(194 57 52 / 8%);
        }

            .sp-has-error label {
                color: #c23934!important;
            }

	/*** CARD ***/
		.sp-services {
			text-align: center;
			box-shadow: 0 10px 20px rgba(0,0,0,.04);
			border-radius: 10px;
			background-color: #FFFFFF;
			margin-top: 26px;
		} 

		.sp-services-content {
			position: relative;
			padding: 13px 13px 24px 13px;
		}

		.sp-services-img {
			border-radius: 8px;
		} 

		.sp-field img {
			position: absolute;
    		right: 10px;
    		top: 43%;
		} 



		.sp-service-title {
			margin-top: -25px;
			padding-bottom: .5rem;
			font-weight: 500;
		}

		.sp-services-button {
			display: block;
			width: 100%;
			background: #ffffff;
			margin: 0 auto;
			border: 1px solid rgba(0,0,0,.15);
			padding: 1em;
			margin-top: 1.35rem;
			border-radius: 5px;
			font-weight: 500;
			font-size: 14px;
			letter-spacing: 0.5px;
		}

		.sp-services-button:hover {
			background-color: #f46b41;
			color: #FFFFFF;
		}

		.sp-services-button:focus {
			border: initial;
		}

	/*** PROGRESS BAR ***/
		.sp-progress {
			position: absolute;
			top: 1.75rem;
			left: calc(0.5% + 6px);
			transform: translateX(-50%);
			background: initial;
			box-shadow: initial;
    		text-align: center;
		}

			.sp-progress-1 {
				left: calc(0.5% + 6px);
			}

			.sp-progress-2 {
				left: calc(50% + 6px);
			}

			/*.sp-progress-2-new {
				left: calc(50% + 6px);
			}*/

			.sp-progress-3 {
				left: calc(97% + 6px);
    			min-width: 155px;
			}
			
			/*.sp-progress-4 {
				left: calc(98% + 6px);
				min-width: 160px;
			}*/

			.sp-progress p {
				color: #adadad;
			}

		.slds-progress__item.slds-is-completed .slds-progress__marker,
		.slds-progress__item.slds-is-active .slds-progress__marker,
		.slds-progress__item.slds-is-active .slds-progress__marker:focus, 
		.slds-progress__item.slds-is-active .slds-progress__marker:hover {
			border-color: #f46b41;
		}

		.slds-progress__item.slds-is-active p {
			color: #f46b41;
		}

		.slds-progress__marker {
			width: 1.5rem;
			height: 1.5rem;
		}

		.slds-progress__item.slds-is-completed .slds-progress__marker {
			background-color: #f46b41;
		}

    /*** TOAST ***/
        .slds-notify_toast {
    		min-width: initial;
            margin: 0;
            width: -webkit-fill-available;
            width: -moz-fill-available;
            width: fill-available;
    		border-radius: 0.15rem;
        }


	/*** MOBILE ***/
    @media only screen and (max-width: 480px) {
    	.slds-progress {
            max-width: 75%;
		}

        .slds-progress p {
    		font-size: .75rem;		
    	}
    
        .sp-page-heading {
            text-align: center;
        }
    
    	.sp-sidebar-help {
    		margin-top: 30px;
    	}
    	
    	/* progress bar */
    	.sp-progress-1 {
    		left: calc(2% + 6px);
        }
    
    	.sp-progress-3 {
            left: calc(94% + 6px);
            min-width: 91px;
        }
    	
    	/* form */
    	.sp-field textarea {
            padding: inherit;
            min-height: 1rem;
        }sp-field
    	
    	.sp-field select {
            font-size: .95rem;
        }
    
    	/* remove touch action from texareas */
    	.sp-field textarea{
            transition: initial;
            touch-action: initial;
        	padding: .65rem;
        }
    
        .sp-field.sp-field-textarea {
        	padding-top: 1rem;
    		position: initial;
    		display: inline-block;
    		flex-flow: initial;
        }
    		
            .sp-field textarea:placeholder-shown + label {
                padding-left: 0;
                transform: initial;
                text-overflow: initial;
                transform-origin: initial;
                min-width: fit-content;
                top: 0;
                font-size: .85rem;
            }
    		/* label when active */
            .sp-field textarea:not(:placeholder-shown) + label, 
            .sp-field textarea:focus + label {
                top: initial!important;
                transform: none!important;
                padding-left: 0;
                font-size: .85rem;
            }   

	}

	/* MISCELLANEOUS */ 
	/*remove clendar icon in chrome*/
	input[type="date"]::-webkit-calendar-picker-indicator,
	input[type="date"]::-webkit-inner-spin-button {
    	display: none;
	}
	