/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 08-26-2020
 * @last modified by  : Ibrahim Napoles
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   08-26-2020   Ibrahim Napoles   Initial Version
**/
@IsTest
private class WebServiceClassesTest {
    @IsTest
    static void coverWebServiceClasses(){
        Test.startTest();
        dcFedexServiceClient.PackageBarcodes pb = TestDataFactory.coverAllWebServiceClasses();
        
        Test.stopTest();
        System.assertNotEquals(null, pb, '');
    }
}