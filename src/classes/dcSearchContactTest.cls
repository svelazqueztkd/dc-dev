/**
 * @File Name          : dcSearchContactTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcSearchContactTest {
    @isTest
    static void constructorTest1(){
        Test.startTest();
        dcSearchContact search = new dcSearchContact();
        Test.stopTest();
        System.assertNotEquals(null, search, '');
        
    }

    @isTest
    static void constructorTest2(){
        Test.startTest();
        dcSearchContact search = new dcSearchContact(new dcSearchContact());
        Test.stopTest();
        System.assertNotEquals(null, search, '');
    }    
}