/**
 * @File Name          : NumberTOWordConvertion.cls
 * @Description        : Class to convert numbers to string literal
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 13/8/2020 10:22:17 a. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/8/2020   Silvia Velazquez     Initial Version
**/
public with sharing class NumberTOWordConvertion {

    public static integer intNum;     // for integer part
	public static integer decNum;     // for decimal part
	public static String numInWords; // For currency in Words

	// Can be customized to Rupees and Paisa, Pounds and penny, etc based on req.
	public static String bigCurrencySingular    = 'Dollar';
	public static String bigCurrencyplural      = 'Dollars';
	public static String smlCurrencySingular    = 'Cent';
    public static String smlCurrencyplural      = 'Cents';
    public static String zeroCents = 'and 00/100';

	public static String[]  ones = new string[] {  'Zero ',  'One ',  'Two ',  'Three ',  'Four ',  'Five ',  'Six ',  'Seven ',  'Eight ',  'Nine ',  'Ten ',  'Eleven ',  'Twelve ',  'Thirteen ',  'Fourteen ',  'Fifteen ',  'Sixteen ',  'Seventeen ',  'Eighteen ',  'Nineteen ' };
	public static String[]  tens = new string[] {  ' ',  'Ten ',  'Twenty ',  'Thirty ',  'Forty ',  'Fifty ',  'Sixty ',  'Seventy ',  'Eighty ',  'Ninety ' };
	public static integer[] base = new integer[]{0,1,10,100,1000,1000,1000,1000000,1000000,1000000,1000000000};
	public static String[]  unit = new String[] { ' ', ' ', ' ', 'Hundred ', 'Thousand ', 'Thousand ', 'Thousand ', 'Million ', 'Million ', 'Million ', 'Billion ', 'Billion ', 'Billion ', 'Trillion '};


	public static String CurrencyToWordsFormat(Integer num){
		intNum = num;
		if( intNum == 0 ){
           numInWords = 'Zero '; 
        }
		else{
            numInWords = NumberToWords(intNum); 
        }    
        return numInWords + zeroCents;
	}

	public static String CurrencyToWordsFormat(Decimal num){
		String numToString = String.valueOf(num);
		List<String> parts = numToString.split('\\.');
		if( parts != null && parts.size()>0 ){
            if(parts.size() == 1){ //it's an integer
                numInWords = CurrencyToWordsFormat(Integer.valueOf(parts[0]));
            }
            else{
                intNum = Integer.valueOf(parts[0]);
                String decStr = getValidDecimal(parts[1]);
                decNum = Integer.valueOf(decStr);
            
                if( intNum == 0 ){
                    numInWords = 'Zero ';
                }    
                else{
                    numInWords = NumberToWords(intNum); 
                }

                if( decNum == 0 ){
                        numInWords += zeroCents;
                }
                else{
                    numInWords += 'and ' + decStr + '/100';
                }
            }
        }
        System.debug(num+':'+numInWords);
        return numInWords;
	}


	public static String NumberToWords(Integer num){
		String words = '';
		integer digit, den, rem;
		digit = String.valueOf(num).length();
		//System.debug('No. de Digitos: '+digit);
        den = num / base[digit];
        //System.debug('Division  de '+num+'/[10,100,1000] : ' + den);
        rem = Math.mod(num,base[digit]);
        //System.debug('Resto de la Division: '+ rem);
		if( digit == 1 ){
		    words = ones[num];
		}
		else if( digit == 2 ){
            if( den == 1 && rem >0 )
                words = ones[num];
            else{
                words = tens[den];
                if( rem > 0 )
                    words += NumberToWords(rem);
            }
		}
		else if( digit > 2 ){
            words += NumberToWords(den)+unit[digit];
            if( rem > 0 )
               /* if(unit[digit] == 'Hundred ') //one hundred and fifty
                words += 'and '+NumberToWords(rem);
               else */
                words += NumberToWords(rem);
		}
		return words;
    }

    public static String getValidDecimal(String value){
        String result = value;
        if (value != '0' && value != '00' && value.length() > 1 && value.substring(0,1) =='0') { //.05
            result = value.substring(1);  //5
        }
        else{
            if(value != '0' && value.length() == 1){ //.1 ó .2 --> 10 ó 20
                result = value + '0';
            }
        }
        return result;
    }
}