/**
 * @File Name          : dcSearchContactResult.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 27/7/2020 5:11:38 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/7/2020   Silvia Velazquez     Initial Version
**/
public with sharing class dcSearchContactResult {
	@AuraEnabled
	public List<dcContactProcedureSearch> contactResults {get; set;}

/* 	@AuraEnabled
	public Integer contactsCount {get; set;} // Number of contacts
	@AuraEnabled
	public Integer searchCount {get; set;} // Number of search results*/

	public Boolean emptyList {get; set;}
	@AuraEnabled
	public Integer count {get; set;}
	@AuraEnabled
	public Boolean noResult {get; set;}
	@AuraEnabled
	public Boolean errorShow {get; set;}
	@AuraEnabled
	public String errorTitle {get; set;}
	@AuraEnabled
	public String errorMessage {get; set;}
	@AuraEnabled
	public dcSearchContact searchParams; 	
		
	public dcSearchContactResult(dcSearchContact searchContact) {
		contactResults = new List<dcContactProcedureSearch>();
		errorShow = false;
		emptyList = true;
		noResult = false;
		errorTitle = '';
		errorMessage = ''; 
		searchParams = searchContact;
	}

	public dcSearchContactResult(List<dcContactProcedureSearch> searchListResult,dcSearchContact searchContact) {
		contactResults = searchListResult;
		errorShow = false;
		emptyList = contactResults.size() == 0;
		noResult = contactResults.size() == 0;
		errorTitle = '';
		errorMessage = ''; 
		searchParams = searchContact;
	}	
}