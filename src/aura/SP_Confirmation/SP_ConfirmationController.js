({
	initAction : function(cmp, event, helper) {
		console.log('confirmationController v1');
	},

	closeAction : function(cmp, event, helper) {
		helper.close(cmp, helper.CLOSE_ACTION);
	},

	cancelAction : function(cmp, event, helper) {
		helper.close(cmp, helper.CANCEL_ACTION);
	},

	okAction : function(cmp, event, helper) {
		helper.close(cmp, helper.OK_ACTION);
	}
})