({
    serverAction: function(cmp, method, params) {
      var self = this;
  
      return new Promise(function(resolve, reject) {
        var action = cmp.get("c." + method);
  
        if (params != null) action.setParams(params);
  
        action.setCallback(self, function(response) {
          console.log("Begin Callback" + Date(Date.now()));
          var state = response.getState();
  
          if (state == "SUCCESS") resolve.call(this, response.getReturnValue());
          else if (state == "ERROR") {
            var errors = response.getError();
            var title = cmp.get("v.title");
            this.showToast(cmp, title, errors[0].message, "error")
            reject.call(this, errors);
          }
        });
  
        $A.enqueueAction(action);
        console.log("Finish EnqueueAction" + Date(Date.now()));
      });
    },
    isValidDate: function(date){
        var d = new Date(date);
        var msc = d.getMilliseconds();
        
        return (!isNaN(msc));
    },
    formatDatetoUTC: function(date, recivedformat, sep) {
        var 
          mPos = recivedformat.indexOf('m'), 
          dPos = recivedformat.indexOf('d'), 
          yPos = recivedformat.indexOf('y');  
   
        var parts = date.split(sep);

        var 
          month = parts[mPos],
          day =  parts[dPos],
          year = parts[yPos];

        return [year, month, day].join('-'); 
    },
    formatDate: function(date) {  
               
        if (!this.isValidDate(date))  return '';
        
        var d = new Date(date);
        //alert(d);   
        var month = '' + (d.getUTCMonth() + 1),
            day = '' + d.getUTCDate(),
            year = d.getUTCFullYear();
        
        //alert(month); alert(day); alert(year);
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
        
        return [year, month, day].join('-'); 
        
    },
    updateSearchParamsWrapper: function(component) {
       
       var birthdate = component.get("v.birthdate");
       birthdate = this.formatDate(birthdate);
       //alert(birthdate);
      var searchPattern = {
        firstName: component.get("v.firstname"),
        middleName: component.get("v.secondname"),
        lastName: component.get("v.lastname"),
        secondLastName: component.get("v.secondlastname"),
        birthdate: birthdate,
        pageSize: component.get("v.pageSize"),
        offset: component.get("v.offset"),
        sortBy: component.get("v.sortBy"),
        isDescending: component.get("v.isDescending")
      };
  
      component.set("v.searchPattern", searchPattern);
    },
    searchAction: function(component, concat) {
      var self = this;
  
      var search = component.get("v.searchPattern");
      //alert(JSON.stringify(search)); 
      return self
        .serverAction(component, "search", {
          searchPattern: JSON.stringify(search)
        })
        .then(function(results) {
          if (results.errorShow == true) {
            component.set("v.errorShow", results.errorShow);
            component.set("v.errorTitle", results.errorTitle);
            component.set("v.errorMessage", results.errorMessage);
            self.showToast(component,results.errorTitle, results.errorMessage, "error");
          } else {
            self.updateDataList(component, results.contactResults, concat);
            component.set("v.emptyList", results.emptyList);
          }
        });
    },
    showToast: function(component, title, message, type) {
        
        var toastEvent = $A.get("e.force:showToast");
        //alert(toastEvent);
        if (toastEvent){
            toastEvent.setParams({
                        "title": title,
                        "message": message,
                        "type": type,
                        "duration": 1500,
	                });
    		toastEvent.fire();
        }
        else{
             	var cmp = component.find('notifLib');
                component.find('notifLib').showToast({
                    "variant": type,
                    "header":  title,
                    "message": message
                });
        }
    },
    updateDataList: function(component, items, concat) {
      var self = this;
      var newList = items || [];
      if (newList.length == 0) {
        return;
      }
  
      if (concat) {
        var current = component.get("v.resultList") || [];
        newList = current.concat(newList);
        self.prepareRecords(component, "v.resultList", newList);
        var childCmp = component.find("childObjectDataTable");
        childCmp.finishLoadMore();
      } else {
        self.prepareRecords(component, "v.resultList", newList);
      }
        
      //var list = component.get("v.resultList");
      //alert(JSON.stringify(list));  
        
      var newOffset = newList.length;
  
      component.set("v.offset", newOffset);
      var recordCount = component.get("v.recordCount");
  
      var maxRows = component.get("v.maxRows");
  
      var maxLenght =
        recordCount > 0 && recordCount < maxRows ? recordCount : maxRows;
  
      if (newOffset >= maxLenght) {
        component.set("v.infLoading", false);
      }
    },
    prepareRecords: function(component, fileName, records) {
      var self = this;
        
      if (records && records.length) {
        for (var i = 0; i < records.length; i++) {
          
        }
      }
      component.set(fileName, records);
    },
    finishReadingEnter: function(component) {
        var readingEnter = component.get("v.readingEnter");
        return readingEnter.includes("<<<")
    },
    getSearchValues: function(component, event, helper) {
        
        if (!this.finishReadingEnter(component) ) return;
         
        var readingEnter = component.get("v.readingEnter");
        var readingArray = readingEnter.split("<");
        //alert(readingArray);
        
        if (readingArray.lengh < 6)  return; 
        component.set("v.firstname", readingArray[0]);
        component.set("v.secondname", readingArray[1]);
        var lastname = readingArray[2].split(" "); 
        component.set("v.lastname", lastname[0]);
        var secondlastname = lastname[1];
        
        // si el apellido es combinado
        var i;
        for(i=2; i< lastname.lengh; i++)
          secondlastname = secondlastname + " " + lastname[i];        
        component.set("v.secondlastname", secondlastname);
        
        if (this.isValidDate(readingArray[3])){
             readingArray[3] = this.formatDatetoUTC(readingArray[3],"mdy","/");
             //alert(readingArray[3]);
             component.set("v.birthdate", readingArray[3]);
        } 
        var strNumber = readingArray[5].substring(0,9);
        readingArray[5] = strNumber;
        component.set("v.passportnumber", readingArray[5]);
        //alert(strNumber);
        if (this.isValidDate(readingArray[4])){
          readingArray[4] = this.formatDatetoUTC(readingArray[4],"mdy","/");
          //alert(readingArray[4]);
          component.set("v.passportexpirationdate", readingArray[4]);
        }
        component.set("v.automaticSearch",true);
    },
    reset: function(component, helper) {
      component.set("v.loading", true);
  
      component.set("v.readingEnter", "");
      component.set("v.firstname", "");
      component.set("v.secondname", "");
      component.set("v.lastname", "");
      component.set("v.secondlastname", "");
      component.set("v.birthdate", null);
      component.set("v.passportnumber", "");
      component.set("v.passportexpirationdate", null);
        
      component.set("v.pageSize", 25);
      component.set("v.offset", 0);
      component.set("v.sortBy", "Name");
      component.set("v.isDescending", true);
  
      component.set("v.showDataTable", false);
      component.set("v.emptyList", true);
      component.set("v.recordCount", 0);
      component.set("v.infLoading", true);
  
      helper.updateSearchParamsWrapper(component);
  
      var search = component.get("v.searchPattern");
      helper
        .serverAction(component, "reset", {
          searchPattern: JSON.stringify(search)
        })
        .then(function(results) {
          helper.prepareRecords(
            component,
            "v.resultList",
            results.productionList
          );
  
          if (results.errorShow == true) {
            component.set("v.errorTitle", results.errorTitle);
            component.set("v.errorMessage", results.errorMessage);
            helper.showToast(component, results.errorTitle, results.errorMessage, "error");
          }
  
          component.set("v.loading", false);
          var readingEnter = component.find("readingEnter") ;
          readingEnter.focus(); 
        });
    },
    searchAutomaticAction: function(component, event, helper) {
      component.set("v.loading", true);
      component.set("v.showDataTable", true);
      component.set("v.offset", 0);
  
      helper.updateSearchParamsWrapper(component);
  
      console.log("Init Search" + Date(Date.now()));
      var search = component.get("v.searchPattern");
      // alert(JSON.stringify(search));  
      helper
        .serverAction(component, "count", {
          searchPattern: JSON.stringify(search)
        })
        .then(function(results) {
          //alert(JSON.stringify(results));   
          if (results.errorShow == true) {
            component.set("v.errorShow", results.errorShow);
            component.set("v.errorTitle", results.errorTitle);
            component.set("v.errorMessage", results.errorMessage);
            helper.showToast(component, results.errorTitle, results.errorMessage, "error");
          } else {
            
            component.set("v.recordCount", results.count);
            helper.updateDataList(component, results.contactResults, false);
            component.set("v.emptyList", results.noResult);
  
            //alert(JSON.stringify(component.get("v.columnFields"))); 
              
            console.log("Finish Count Server request" + Date(Date.now()));
            component.set("v.loading", false);
            var readingEnter = component.find("readingEnter") ;
            readingEnter.focus();  
          }
        });
        var readingEnter = component.find("readingEnter") ;
        readingEnter.focus(); 
    }
  });