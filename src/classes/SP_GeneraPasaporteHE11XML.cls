/**
 * @description       :
 * @author            : Silvia Velazquez
 * @group             :
 * @last modified on  : 10-15-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log
 * Ver   Date         Author                   Modification
 * 1.0   07-23-2020   William Santana Méndez   Initial Version
 * 1.1   08-21-2020   Ibrahim Napoles          Validate CRUD permission before SOQL/DML operation.
 **/
public with sharing class SP_GeneraPasaporteHE11XML {
    @InvocableMethod(label='Create Pasaporte HE11' description='Create a XML with the information of consular procedures corresponding to the specified Manifest IDs.' category='Procedure__c')
    public static void generaXML(List<String> manifiestos){
        if(!(
             Schema.sObjectType.RecordType.isAccessible() &&
             Schema.sObjectType.RecordType.fields.Id.isAccessible() &&
             Schema.sObjectType.RecordType.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.RecordTypeId.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.First_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Middle_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Last_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Second_Last_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Gender__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Birthday__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Father_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Mother_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Numero_de_Pasaporte__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Skin_Color__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Eyes_Color__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Hair_Color__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Height__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Profession__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.ProfessionCategory__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Nivel_de_Escolaridad__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Title__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Departure_Date__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Immigration_Status__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Birth_Country__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Birth_Province__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Birth_Municipality_City__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.State__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.City__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Phone__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Fax__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Email__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Country__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Postal_Code__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Street__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_State__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Birth_Certificate_Issue_Date__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_City__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_Phone__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_Fax__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_Email__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_Country__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_Postal_Code__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Work_Street__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.ID_Carne__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Birth_Certificate__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Birth_Certificate_Issue_At__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Address_1__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Address_1_Province__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Address_1_City__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Year_1_From__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Year_1_To__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Address_2__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Address_2_Province__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Address_2_City__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Year_2_From__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Residence_Year_2_To__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Reference_Full_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Reference_Address__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Reference_Phone__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Comments__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Category__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Passport_Type__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Phone1__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Phone2__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Reference_Second_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Reference_Last_Name__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Reference_Second_Surname__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.References_Province__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.References_Municipality__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Reference_District__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Manifest__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Manifest__c.isAccessible() &&
             Schema.sObjectType.Manifest__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Manifest__c.fields.Name.isAccessible())) {
            DCException.throwPermiException('SP_GeneraPasaporteHE11XML.generaXML');
        }

        List<Procedure__c> procedureList = [SELECT id, Name, First_Name__c, Middle_Name__c, Last_Name__c,
                                                    Second_Last_Name__c, Gender__c,
                                                    Birthday__c, Father_Name__c, Mother_Name__c, 
                                                    Numero_de_Pasaporte__c, Skin_Color__c, 
                                                    Eyes_Color__c, Hair_Color__c,
                                                    Height__c, Profession__c, ProfessionCategory__c, 
                                                    Nivel_de_Escolaridad__c, Title__c, 
                                                    Departure_Date__c,
                                                    Immigration_Status__c, Birth_Country__c, 
                                                    Birth_Province__c, Birth_Municipality_City__c, 
                                                    State__c, City__c,
                                                    Phone__c, Fax__c, Email__c, Residence_Country__c,
                                                    Postal_Code__c, Street__c, Work_State__c,
                                                    Birth_Certificate_Issue_Date__c, Work_City__c, 
                                                    Work_Phone__c, Work_Fax__c, Work_Email__c, 
                                                    Work_Country__c,
                                                    Work_Name__c, Work_Postal_Code__c, 
                                                    Work_Street__c, ID_Carne__c, Birth_Certificate__c, 
                                                    Birth_Certificate_Issue_At__c,
                                                    Residence_Address_1__c, 
                                                    Residence_Address_1_Province__c, 
                                                    Residence_Address_1_City__c, 
                                                    Residence_Year_1_From__c,
                                                    Residence_Year_1_To__c, 
                                                    Residence_Address_2__c,
                                                    Residence_Address_2_Province__c, 
                                                    Residence_Address_2_City__c,
                                                    Residence_Year_2_From__c, 
                                                    Residence_Year_2_To__c, 
                                                    Reference_Full_Name__c, Reference_Address__c,
                                                    Reference_Phone__c, Comments__c, Category__c, 
                                                    Passport_Type__c, Phone1__c, Phone2__c, 
                                                    Reference_Second_Name__c,
                                                    Reference_Last_Name__c, 
                                                    Reference_Second_Surname__c, 
                                                    References_Province__c, 
                                                    References_Municipality__c, 
                                                    Reference_District__c,
                                                    Manifest__r.Name
                                            FROM Procedure__c
                                            WHERE (Manifest__c in :manifiestos and RecordType.Name='Visa HE-11') LIMIT 30];

        Dom.Document doc = new Dom.Document();
        Dom.Xmlnode rootNode = doc.createRootElement('Tramites', null, null);

        string rootName = '';
        for (Procedure__c eachProcedure : procedureList) {
            rootName = 'PE-' + eachProcedure.Manifest__r.Name.substring(1); 
            system.debug('root Name --> ' + rootName);
            Dom.Xmlnode pasaporteNode = rootNode.addChildElement(rootName, null, null);

            Dom.Xmlnode vchNombre1 = pasaporteNode.addChildElement('vch_nombre1', null, null);
            String valorVchNombre1 = String.isNotBlank(eachProcedure.First_Name__c) ? eachProcedure.First_Name__c.toUpperCase() : '';
            vchNombre1.addTextNode(valorVchNombre1);

            Dom.Xmlnode vchNombre2 = pasaporteNode.addChildElement('vch_nombre2', null, null);
            String valorVchNombre2 = String.isNotBlank(eachProcedure.Middle_Name__c) ? eachProcedure.Middle_Name__c.toUpperCase() : '';
            vchNombre2.addTextNode(valorVchNombre2);

            Dom.Xmlnode vchApellido1 = pasaporteNode.addChildElement('vch_apellido1', null, null);
            String valorVchApellido1 = String.isNotBlank(eachProcedure.Last_Name__c) ? eachProcedure.Last_Name__c.toUpperCase() : '';
            vchApellido1.addTextNode(valorVchApellido1);

            Dom.Xmlnode vchApellido2 = pasaporteNode.addChildElement('vch_apellido2', null, null);
            String valorVchApellido2 = String.isNotBlank(eachProcedure.Second_Last_Name__c) ? eachProcedure.Second_Last_Name__c.toUpperCase() : '';
            vchApellido2.addTextNode(valorVchApellido2);

            Dom.Xmlnode bitSexo = pasaporteNode.addChildElement('bit_sexo', null, null);
            Map<String, String> sexo = new Map<String, String>();
            String valorSexo='';
            sexo.put('Masculino', 'true');
            sexo.put('Femenino', 'false');

            if(sexo.containsKey(eachProcedure.Gender__c)){
                valorSexo= sexo.get(eachProcedure.Gender__c).toUpperCase();
            }
            bitSexo.addTextNode(valorSexo);
            //}

            Dom.Xmlnode dtmFnacimiento = pasaporteNode.addChildElement('dtm_fNacimiento', null, null);
            String fecha = '';
            if(eachProcedure.Birthday__c!=null) {
                fecha = XMLresources.formateaFecha(eachProcedure.Birthday__c);
            }
            dtmFnacimiento.addTextNode(fecha);

            Dom.Xmlnode intCategoria = pasaporteNode.addChildElement('int_categoria', null, null);
            Map<String, Integer> categoria = new Map<String, Integer>();
            String valorCategoria='';
            categoria.put('Permanente', 43);
            categoria.put('Extranjero', 93);
            categoria.put('Temporal', 94);

            if(categoria.containsKey(eachProcedure.Category__c)){
                valorCategoria=String.valueOf(categoria.get(eachProcedure.Category__c));
            }
            intCategoria.addTextNode(valorCategoria);
            // }

            Dom.Xmlnode vchNombrePadre = pasaporteNode.addChildElement('vch_nombrePadre', null, null);
            String valorVchNombrePadre = '';
            if(String.isNotBlank(eachProcedure.Father_Name__c)) {
                valorVchNombrePadre=eachProcedure.Father_Name__c.toUpperCase();
            }
            vchNombrePadre.addTextNode(valorVchNombrePadre);

            Dom.Xmlnode vchNombreMadre = pasaporteNode.addChildElement('vch_nombreMadre', null, null);
            String valorVchNombreMadre = String.isNotBlank(eachProcedure.Mother_Name__c) ? eachProcedure.Mother_Name__c.toUpperCase() : '';
            vchNombreMadre.addTextNode(valorVchNombreMadre);

            Dom.Xmlnode vchPasaporte = pasaporteNode.addChildElement('vch_pasaporte', null, null);
            string valorVchPasaporte = String.isNotBlank(eachProcedure.Numero_de_Pasaporte__c) ? eachProcedure.Numero_de_Pasaporte__c.toUpperCase() : '';
            vchPasaporte.addTextNode(valorVchPasaporte);

            Dom.Xmlnode bitCubext = pasaporteNode.addChildElement('bit_cubext', null, null);
            bitCubext.addTextNode('false');

            Dom.Xmlnode intPiel = pasaporteNode.addChildElement('int_piel', null, null);
            Map<String, Integer> piel = new Map<String, Integer>();
            String valorPiel='';
            piel.put('Blanca', 3);
            piel.put('Negro', 4);
            piel.put('Mulata', 5);
            piel.put('Albina', 203);
            piel.put('Amarilla', 204);

            if(piel.containsKey(eachProcedure.Skin_Color__c)){
                valorPiel=String.valueOf(piel.get(eachProcedure.Skin_Color__c));
            }
            intPiel.addTextNode(valorPiel);

            Dom.Xmlnode intOjos = pasaporteNode.addChildElement('int_ojos', null, null);
            Map<String, Integer> ojos = new Map<String, Integer>();
            String valorOjos='';
            ojos.put('Negros', 6);
            ojos.put('Pardos', 512);
            ojos.put('Claros', 514);

            if(ojos.containsKey(eachProcedure.Eyes_Color__c)){
                valorOjos= String.valueOf(ojos.get(eachProcedure.Eyes_Color__c));
            }
            intOjos.addTextNode(valorOjos);

            Dom.Xmlnode intPelo = pasaporteNode.addChildElement('int_pelo', null, null);
            Map<String, Integer> pelo = new Map<String, Integer>();
            String valorPelo='';
            pelo.put('Negro', 12);
            pelo.put('Rubio', 13);
            pelo.put('Rojo', 14);
            pelo.put('Castaño', 15);
            pelo.put('Otro', 18);
            pelo.put('Canoso', 205);

            if(pelo.containsKey(eachProcedure.Hair_Color__c)){
                valorPelo=String.valueOf(pelo.get(eachProcedure.Hair_Color__c));
            }
            intPelo.addTextNode(valorPelo);

            Dom.Xmlnode fltEstatura = pasaporteNode.addChildElement('flt_estatura', null, null);
            String strEstatura = '';
            if(eachProcedure.Height__c!=null) {
                strEstatura = String.valueOf(eachProcedure.Height__c);
            }
            fltEstatura.addTextNode(strEstatura);

            Dom.Xmlnode intProfesion = pasaporteNode.addChildElement('int_profesion', null, null);
            Map<String, Integer> profesion = new Map<String, Integer>();
            String valorProfesion='';
            profesion.put('Abogado', 861);
            profesion.put('Ama de Casa', 862);
            profesion.put('Artista', 863);
            profesion.put('Campesino', 864);
            profesion.put('Cientifico', 865);
            profesion.put('Comerciante', 866);
            profesion.put('Deportista', 867);
            profesion.put('Diplomático', 868);
            profesion.put('Empleado', 869);
            profesion.put('Empresario', 870);
            profesion.put('Enfermera', 871);
            profesion.put('Escritor', 872);
            profesion.put('Estudiante', 873);
            profesion.put('Funcionario', 874);
            profesion.put('Informático Superior', 875);
            profesion.put('Informático Técnico', 876);
            profesion.put('Ingeniero', 877);
            profesion.put('Intelectual', 878);
            profesion.put('Licenciado', 879);
            profesion.put('Maestro', 880);
            profesion.put('Medico', 881);
            profesion.put('Obrero', 882);
            profesion.put('Periodista', 883);
            profesion.put('Político', 884);
            profesion.put('Profesor', 885);
            profesion.put('Religioso', 886);
            profesion.put('Técnico', 887);
            profesion.put('Otra', 1952);

            if(profesion.containsKey(eachProcedure.Profession__c)){
                valorProfesion=String.valueOf(profesion.get(eachProcedure.Profession__c));
            }
            intProfesion.addTextNode(valorProfesion);

            Dom.Xmlnode intCatprofesion = pasaporteNode.addChildElement('int_catprofesion', null, null);
            Map<String, Integer> catProfesion = new Map<String, Integer>();
            String valorCatProfesion='';
            catProfesion.put('Otra', 144);
            catProfesion.put('Cuenta Propista', 856);
            catProfesion.put('Educacion', 857);
            catProfesion.put('Salud', 858);
            catProfesion.put('Deporte', 859);
            catProfesion.put('Turismo', 860);

            if(catProfesion.containsKey(eachProcedure.ProfessionCategory__c)){
                valorCatProfesion=String.valueOf(catProfesion.get(eachProcedure.ProfessionCategory__c));
            }
            intCatprofesion.addTextNode(valorCatProfesion);

            Dom.Xmlnode intNivelescolar = pasaporteNode.addChildElement('int_nivelescolar', null, null);
            Map<String, Integer> nivelEscolar = new Map<String, Integer>();
            String valorNivelEscolar='';
            nivelEscolar.put('Universitario', 23);
            nivelEscolar.put('Tec Medio', 25);
            nivelEscolar.put('Secundario', 508);
            nivelEscolar.put('Primario', 509);
            nivelEscolar.put('Pre- Universitario', 510);
            nivelEscolar.put('Analfabeto', 511);

            if(nivelEscolar.containsKey(eachProcedure.Nivel_de_Escolaridad__c)){
                valorNivelEscolar=String.valueOf(nivelEscolar.get(eachProcedure.Nivel_de_Escolaridad__c));
            }
            intNivelescolar.addTextNode(valorNivelEscolar);

            Dom.Xmlnode intOcupacion = pasaporteNode.addChildElement('int_ocupacion', null, null);
            String valorIntOcupacion = String.isNotBlank(eachProcedure.Title__c) ? eachProcedure.Title__c.toUpperCase() : '';
            intOcupacion.addTextNode(valorIntOcupacion);

            Dom.Xmlnode dtmFsalida = pasaporteNode.addChildElement('dtm_fSalida', null, null);
            String strFSalida = (eachProcedure.Departure_Date__c !=null) ? XMLresources.formateaFecha(eachProcedure.Departure_Date__c) : '';
            dtmFsalida.addTextNode(strFSalida);

            Dom.Xmlnode intCatmigSalida = pasaporteNode.addChildElement('int_catmigSalida', null, null);
            Map<String, Integer> catMigSalida = new Map<String, Integer>();
            String valorCatMigSalida='';
            catMigSalida.put('Permiso de Residencia en el Exterior', 80); //PRE
            catMigSalida.put('Permiso de Salida Indefinido', 82); //PSI
            catMigSalida.put('Asunto Oficial', 84);
            catMigSalida.put('Permiso de Viaje al Exterior', 85); //PVE
            catMigSalida.put('Permiso de Viaje Temporal', 86); // PVT   
            catMigSalida.put('Salida Ilegal', 240);
            catMigSalida.put('Permiso Emigración', 908);

            if(catMigSalida.containsKey(eachProcedure.Immigration_Status__c))
                valorCatMigSalida=String.valueOf(catMigSalida.get(eachProcedure.Immigration_Status__c));
            //}
            intCatmigSalida.addTextNode(valorCatMigSalida);
            // }

            //modificado por mapeo
            Dom.Xmlnode intPaisnac = pasaporteNode.addChildElement('int_paisnac', null, null);
            String valorIntPaisnac = (eachProcedure.Birth_Country__c!=null) ? String.valueOf(XMLresources.mapPais(eachProcedure.Birth_Country__c)) : '';
            intPaisnac.addTextNode(valorIntPaisnac);

            Dom.Xmlnode intProvnac = pasaporteNode.addChildElement('int_provnac', null, null);
            String valorProvNac=(eachProcedure.Birth_Province__c!=null) ? String.valueOf(XMLresources.mapProvincia(eachProcedure.Birth_Province__c)) : '';
            intProvnac.addTextNode(valorProvNac);

            Dom.Xmlnode intMuninac = pasaporteNode.addChildElement('int_muninac', null, null);
            String valorMuniNac= (eachProcedure.Birth_Municipality_City__c!=null) ? String.valueOf(XMLresources.mapMunicipio(eachProcedure.Birth_Municipality_City__c, eachProcedure.Birth_Province__c)) : '';
            intMuninac.addTextNode(valorMuniNac);

            Dom.Xmlnode dirextIntEstado = pasaporteNode.addChildElement('dirext_int_estado', null, null);
            String valorDirextIntEstado = String.isNotBlank(eachProcedure.State__c) ? String.valueOf(XMLresources.mapEstado(eachProcedure.State__c)) : '';
            dirextIntEstado.addTextNode(valorDirextIntEstado);

            Dom.Xmlnode dirextIntCiudad = pasaporteNode.addChildElement('dirext_int_ciudad', null, null);
            dcCityCodes cityCodes = new dcCityCodes();
            String valorDirextIntCiudad= String.isNotBlank(eachProcedure.City__c) ? cityCodes.getCityCode(eachProcedure.City__c.toUpperCase()) : '';
            dirextIntCiudad.addTextNode(valorDirextIntCiudad);

            Dom.Xmlnode dirextVchTelefono = pasaporteNode.addChildElement('dirext_vch_telefono', null, null);
            String valorTelefono = String.isNotBlank(eachProcedure.Phone__c) ? XMLresources.formatPhone(eachProcedure.Phone__c) : '';
            dirextVchTelefono.addTextNode(valorTelefono);

            Dom.Xmlnode dirextVchFax = pasaporteNode.addChildElement('dirext_vch_fax', null, null);
            String valorFax = String.isNotBlank(eachProcedure.Fax__c) ? XMLresources.formatPhone(eachProcedure.Fax__c) : '';
            dirextVchFax.addTextNode(valorFax);

            Dom.Xmlnode dirextVchEmail = pasaporteNode.addChildElement('dirext_vch_email', null, null);
            String valorDirextVchEmail = String.isNotBlank(eachProcedure.Email__c) ? eachProcedure.Email__c : '';
            dirextVchEmail.addTextNode(valorDirextVchEmail);

            Dom.Xmlnode dirextIntCodPostal = pasaporteNode.addChildElement('dirext_int_codPostal', null, null);
            String valorPostal = String.isNotBlank(eachProcedure.Postal_Code__c) ? eachProcedure.Postal_Code__c : '';
            dirextIntCodPostal.addTextNode(valorPostal);

            Dom.Xmlnode dirextVchDireccion = pasaporteNode.addChildElement('dirext_vch_direccion', null, null);
            String valordirextVchDireccion = String.isNotBlank(eachProcedure.Street__c) ? eachProcedure.Street__c.toUpperCase() : '';
            dirextVchDireccion.addTextNode(valordirextVchDireccion);

            Dom.Xmlnode dirtrabajoIntEstado = pasaporteNode.addChildElement('dirtrabajo_int_estado', null, null);
            String valordirtrabajoIntEstado = String.isNotBlank(eachProcedure.Work_State__c) ? String.valueOf(XMLresources.mapEstado(eachProcedure.Work_State__c)) : '';
            dirtrabajoIntEstado.addTextNode(valordirtrabajoIntEstado);

            Dom.Xmlnode dirtrabajoIntCiudad = pasaporteNode.addChildElement('dirtrabajo_int_ciudad', null, null);
            String valordirtrabajoIntCiudad = String.isNotBlank(eachProcedure.Work_City__c) ? cityCodes.getCityCode(eachProcedure.Work_City__c.toUpperCase()) : '';
            dirtrabajoIntCiudad.addTextNode(valordirtrabajoIntCiudad);

            Dom.Xmlnode dirtrabajoVchTelefono = pasaporteNode.addChildElement('dirtrabajo_vch_telefono', null, null);
            String valordirtrabajoVchTelefono = String.isNotBlank(eachProcedure.Work_Phone__c) ? XMLresources.formatPhone(eachProcedure.Work_Phone__c) : '';
            dirtrabajoVchTelefono.addTextNode(valordirtrabajoVchTelefono);

            Dom.Xmlnode dirtrabajoVchFax = pasaporteNode.addChildElement('dirtrabajo_vch_fax', null, null);
            String valordirtrabajoVchFax = String.isNotBlank(eachProcedure.Work_Fax__c) ? XMLresources.formatPhone(eachProcedure.Work_Fax__c) : '';
            dirtrabajoVchFax.addTextNode(valordirtrabajoVchFax);

            Dom.Xmlnode dirtrabajoVchEmail = pasaporteNode.addChildElement('dirtrabajo_vch_email', null, null);
            String valordirtrabajoVchEmail = String.isNotBlank(eachProcedure.Work_Email__c) ? eachProcedure.Work_Email__c : '';
            dirtrabajoVchEmail.addTextNode(valordirtrabajoVchEmail);

             //modificado por mapeo
             Dom.Xmlnode dirtrabajoIntPais = pasaporteNode.addChildElement('dirtrabajo_int_pais', null, null);
             String valordirtrabajoIntPais = String.isNotBlank(eachProcedure.Work_Country__c) ? String.valueOf(XMLresources.mapPais(eachProcedure.Work_Country__c)) : '';
             dirtrabajoIntPais.addTextNode(valordirtrabajoIntPais);

            Dom.Xmlnode vchCentrotrabajo = pasaporteNode.addChildElement('vch_centrotrabajo', null, null);
            String valorvchCentrotrabajo = String.isNotBlank(eachProcedure.Work_Name__c) ? eachProcedure.Work_Name__c.toUpperCase() : '';
            vchCentrotrabajo.addTextNode(valorvchCentrotrabajo);

            Dom.Xmlnode dirtrabajoIntCodPostal = pasaporteNode.addChildElement('dirtrabajo_int_codPostal', null, null);
            String valordirtrabajoIntCodPostal = String.isNotBlank(eachProcedure.Work_Postal_Code__c) ? eachProcedure.Work_Postal_Code__c : '';
            dirtrabajoIntCodPostal.addTextNode(valordirtrabajoIntCodPostal);
 
            Dom.Xmlnode dirtrabajoVchDireccion = pasaporteNode.addChildElement('dirtrabajo_vch_direccion', null, null);
            String valordirtrabajoVchDireccion = String.isNotBlank(eachProcedure.Work_Street__c) ? eachProcedure.Work_Street__c.toUpperCase() : '';
            dirtrabajoVchDireccion.addTextNode(valordirtrabajoVchDireccion);

            //agregar <int_clasificacion>99</int_clasificacion>
            Dom.Xmlnode intClasificacion = pasaporteNode.addChildElement('int_clasificacion', null, null);
            String valorintClasificacion = String.isNotBlank(eachProcedure.Reference_Full_Name__c) ? eachProcedure.Reference_Full_Name__c.toUpperCase() : '';
            intClasificacion.addTextNode(valorintClasificacion);

            Dom.Xmlnode refCubaVchNombre1 = pasaporteNode.addChildElement('refCuba_vch_nombre1', null, null);
            if(eachProcedure.Reference_Full_Name__c==null) {
                refCubaVchNombre1.addTextNode('');
            }
            else {
                String valorrefCubaVchNombre1=eachProcedure.Reference_Full_Name__c;
                refCubaVchNombre1.addTextNode(valorrefCubaVchNombre1.toUpperCase());
            }

            Dom.Xmlnode refCubaVchNombre2 = pasaporteNode.addChildElement('refCuba_vch_nombre2', null, null);
            if(eachProcedure.Reference_Second_Name__c==null)
            {
                refCubaVchNombre2.addTextNode('');
            }
            else
            {
                String valorrefCubaVchNombre2=eachProcedure.Reference_Second_Name__c;
                refCubaVchNombre2.addTextNode(valorrefCubaVchNombre2.toUpperCase());
            }

            Dom.Xmlnode refCubaVchApellido1 = pasaporteNode.addChildElement('refCuba_vch_apellido1', null, null);
            if(eachProcedure.Reference_Last_Name__c==null)
            {
                refCubaVchApellido1.addTextNode('');
            }
            else
            {
                String valorrefCubaVchApellido1=eachProcedure.Reference_Last_Name__c;
                refCubaVchApellido1.addTextNode(valorrefCubaVchApellido1.toUpperCase());
            }

            Dom.Xmlnode refCubaVchApellido2 = pasaporteNode.addChildElement('refCuba_vch_apellido2', null, null);
            if(eachProcedure.Reference_Second_Surname__c==null) {
                refCubaVchApellido2.addTextNode('');
            }
            else {
                String valorrefCubaVchApellido2=eachProcedure.Reference_Second_Surname__c;
                refCubaVchApellido2.addTextNode(valorrefCubaVchApellido2.toUpperCase());
            }


            Dom.Xmlnode refCubaVchDireccion = pasaporteNode.addChildElement('refCuba_vch_direccion', null, null);
            if(eachProcedure.Reference_Address__c==null) {
                refCubaVchDireccion.addTextNode('');
            }
            else {
                String valorrefCubaVchDireccion=eachProcedure.Reference_Address__c;
                refCubaVchDireccion.addTextNode(valorrefCubaVchDireccion.toUpperCase());
            }

            Dom.Xmlnode refCubaIntProvincia = pasaporteNode.addChildElement('refCuba_int_provincia', null, null);
            String valorrefCubaIntProvincia='';
            if(eachProcedure.References_Province__c==null)
            {
                refCubaIntProvincia.addTextNode('');
            }
            else {
                // refCubaIntProvincia.addTextNode(eachProcedure.References_Province__c);
                valorrefCubaIntProvincia=String.valueOf(XMLresources.mapProvincia(eachProcedure.References_Province__c));
                refCubaIntProvincia.addTextNode(valorrefCubaIntProvincia);
            }

            Dom.Xmlnode refCubaIntMunicipio = pasaporteNode.addChildElement('refCuba_int_municipio', null, null);
            String valorrefCubaIntMunicipio='';
            if(eachProcedure.References_Municipality__c==null)
            {
                refCubaIntMunicipio.addTextNode('');
            }
            else
            {
                //refCubaIntMunicipio.addTextNode(eachProcedure.References_Municipality__c);
                valorrefCubaIntMunicipio=String.valueOf(XMLresources.mapMunicipio(eachProcedure.References_Municipality__c, eachProcedure.References_Province__c));
                refCubaIntMunicipio.addTextNode(valorrefCubaIntMunicipio);
            }

            Dom.Xmlnode refCubaVchBarrio = pasaporteNode.addChildElement('refCuba_vch_barrio', null, null);
            if(eachProcedure.Reference_District__c==null)
            {
                refCubaVchBarrio.addTextNode('');
            }
            else {
                String valorrefCubaVchBarrio=eachProcedure.Reference_District__c;
                refCubaVchBarrio.addTextNode(valorrefCubaVchBarrio.toUpperCase());
            }

            Dom.Xmlnode refCubaVchTelefono = pasaporteNode.addChildElement('refCuba_vch_telefono', null, null);
            if(eachProcedure.Reference_Phone__c==null) {
                refCubaVchTelefono.addTextNode('');
            }
            else {
                String validPhone = XMLresources.formatPhone(eachProcedure.Reference_Phone__c);
                refCubaVchTelefono.addTextNode(validPhone);
            }

            Dom.Xmlnode vchObservaciones = pasaporteNode.addChildElement('vch_observaciones', null, null);
            if(eachProcedure.Comments__c==null) {
                vchObservaciones.addTextNode('');
            }
            else {
                String valorvchObservaciones=eachProcedure.Comments__c;
                vchObservaciones.addTextNode(valorvchObservaciones.toUpperCase());
            }

            Dom.Xmlnode bitTrnps = pasaporteNode.addChildElement('bit_trnps', null, null);
            bitTrnps.addTextNode('true');

            Dom.Xmlnode vchCodservagencia = pasaporteNode.addChildElement('vch_codservagencia', null, null);
            if(eachProcedure.Name==null) {
                vchCodservagencia.addTextNode('');
            }
            else {
                String procedureName=XMLresources.procedureNumber(eachProcedure.Name);
                vchCodservagencia.addTextNode('p'+ procedureName);
            }

            Dom.Xmlnode intRecibidopor = pasaporteNode.addChildElement('int_recibidopor', null, null);
            intRecibidopor.addTextNode('32537');

            Dom.Xmlnode intCorreoenvio = pasaporteNode.addChildElement('int_correoenvio', null, null);
            intCorreoenvio.addTextNode('32537');
        }


        String xmlString = doc.toXmlString();
        xmlString=xmlString.replace('<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0" encoding="iso-8859-1" standalone="yes" ?>');
        System.debug('xmlString =' + xmlString);

        XMLresources.addFileXML(manifiestos, xmlString);

    }
}