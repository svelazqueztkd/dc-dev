({
    init: function(component, event, helper) {
      /* console.log("INIT SP_SEARC v.2"); */
      component.set("v.loading", true);
      helper.updateSearchParamsWrapper(component);
  
      component.set("v.columnFields", [
        {
          label: "Nombre",
          fieldName: "firstName",
          type: "text"       
        },
        {
          label: "Segundo Nombre",
          fieldName: "middleName",
          type: "text"        
        },
        {
          label: "Primer Apellido",
          fieldName: "lastName",
          type: "text"      
        },
          {
          label: "Segundo Apellido",
          fieldName: "secondLastName",
          type: "text"        
        },
        {
          label: "Fecha de Nacimiento",
          fieldName: "birthdate",
          type: "datetime"
        },
        {
          label: "Sexo",
          fieldName: "gender",
          type: "text",
        },
        {
          label: "Numero de Tramite",
          fieldName: "procedureNameLink",
          type: "url",
          typeAttributes: {
          		label: { fieldName: "procedureName" },
         		target: "_self"
          },
          sortable: true
        },
        {
          label: "Agencia",
          fieldName: "procedureAgency",
          type: "text"
        },
        {
          label: "Fecha del Tramite",
          fieldName: "procedureDate",
          type: "text",
          sortable: true
        },
        {
          label: "Tipo de Tramite",
          fieldName: "procedureType",
          type: "text"
        },
        {
          label: "Estado del Tramite",
          fieldName: "procedureStatus",
          type: "text"
        },
        {
          type: "button",  
          typeAttributes: {
                label: "Completar",
                name:  "Completar",
                title: "Completar",
                disabled: { fieldName: "consuladoStatus" },
                value: "Completar"
          }
        }
      ]);
  
      var search = component.get("v.searchPattern");
      helper
        .serverAction(component, "initData", {
          searchPattern: JSON.stringify(search)
        })
        .then(function(results) {
  
          helper.updateSearchParamsWrapper(component);
  
          if (results.errorShow == true) {
            helper.showToast(component, results.errorTitle, results.errorMessage, "error");
          }
 
          component.set("v.loading", false);
          component.set("v.showConfirmation", false);
          var readingEnter = component.find("readingEnter") ;
          readingEnter.focus();  
        });
    },
    handleSearchChange: function(component, event, helper) {
      helper.updateSearchParamsWrapper(component);
    },
    searchAction: function(component, event, helper) {
        helper.searchAutomaticAction(component, event, helper); 
    },
    resetAction: function(component, event, helper) {
         helper.reset(component, helper);
         var readingEnter = component.find("readingEnter") ;
         readingEnter.focus(); 
    },
    handleComponentEvent: function(component, event, helper) {
      if (event.getParam("isSort")) {
        var sortFieldName = "";
        //alert(event.getParam("sortBy"));
        switch (event.getParam("sortBy")) {
          
          case "procedureNameLink": 
            sortFieldName = "Name";
            break; 
          case "procedureDate":  
            sortFieldName = "CreatedDate";
            break;
          default:
            sortFieldName = event.getParam("sortBy");
            break;
        }
  
        var searchPattern = {
          sortBy: sortFieldName,
          sortDirection: event.getParam("sortDirection")
        };
        component.set("v.sortBy", searchPattern.sortBy);
  
        component.set("v.sortDirection", searchPattern.sortDirection);
        component.set(
          "v.isDescending",
          searchPattern.sortDirection == "desc" ? true : false
        );
        component.set("v.offset", 0);
        helper.updateSearchParamsWrapper(component, {});
        helper.searchAction(component, false);
      }
      if (event.getParam("isLoadMore")) {
        helper.updateSearchParamsWrapper(component, {});
        helper.searchAction(component, true);
      }
    },
    handleDataTableAction: function(component, event, helper) {
        var row = event.getParam('row');
        //guardar en el componente los valores de fila seleccionada
        component.set("v.selectedContactId", row.contactId);
        component.set("v.selectedProcedureId", row.procedureId);
        component.set("v.selectedProcedureName", row.procedureName);
        //alert(JSON.stringify(row));
        // setaer el mensaje y activar el modal
        var message = component.get("v.confirmationMsg");
        message = message.replace("[no]", row.procedureName);
        component.set("v.confirmationMsg", message);
        var showModal = true;
        component.set("v.showConfirmation", showModal);
    },
    handleReadingEnter: function(component, event, helper) {
        var self = this;
        helper.getSearchValues(component, event, helper);
        var automaticSearch = component.get("v.automaticSearch");
        
        if (automaticSearch){
            helper.searchAutomaticAction(component, event, helper);
        }        
    },
    handleConfirmationEvent : function(component, event, helper) {
        //alert('entre al evento confirmacion');
        var action = event.getParam("action");
        //alert('@@@ ==> ' + action);
        var showModal = false;
        component.set("v.showConfirmation", showModal);
        //if action = 2 user selecciono OK, ejecutar Completar Tramite
        if (action == 2){
            var contactId = component.get("v.selectedContactId");
            var procedureId= component.get("v.selectedProcedureId");
            var passportNumber = component.get("v.passportnumber");
            var expirationDate = helper.formatDate(component.get("v.passportexpirationdate"));
            //alert(component.get("v.passportexpirationdate"));
            helper
            .serverAction(component, "updateProcedure", {
                  contactId : contactId,
                  procedureId : procedureId,
                  passportNumber: passportNumber,
                  expirationDate: expirationDate 
            })
            .then(function(results) {
                 //alert(results);
               
                 if (results === true) {
                     //alert('estoy en el if de toast message');
                     var message = component.get("v.successmessage");
                     var title = component.get("v.title");
                     //alert(message);
                     helper.showToast(component, title, message, "info");
                    //hacer un reset de la pagina 
                     helper.reset(component, helper);   
                     var readingEnter = component.find("readingEnter") ;
                     readingEnter.focus(); 
                 }
                 else{
                     var message = component.get("v.completeactionerrormessage");
                     var title = component.get("v.title");
                     helper.showToast(component, title, message, "error");
                     var readingEnter = component.find("readingEnter") ;
                     readingEnter.focus(); 
                 }                  
            });
        }            
    }
  });