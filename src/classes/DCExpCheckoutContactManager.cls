/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-19-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-05-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCExpCheckoutContactManager {

	/**
	* @description Retrieve an existent or new contact given a Fullname value and a birthdate value.
	* @author Silvia Velazquez | 10-06-2020 
	* @param Procedure__c procedure 
	* @return Contact 
    **/
	public static Contact getContact(Procedure__c procedure){				   
		if(!(Schema.sObjectType.Contact.isAccessible() &&
			Schema.sObjectType.Contact.fields.Id.isAccessible())) {
			DCException.throwPermiException('DCExpCheckoutContactManager.getContact');
		}
		List<Contact> contacts = [SELECT Id,Birth_Country__c,Birth_Municipality_CityT__c,Birth_Municipality_City__c, 
                                        Birth_Province__c,Birth_ProvinceT__c,Birthdate,Category__c,Departure_Date__c,Email,
                                        Eyes_Color__c,Hair_Color__c,Father_Name__c,FirstName,Gender__c,
                                        Height__c,Immigration_Status__c,LastName,MailingCity,MailingCountryCode,
                                        MailingPostalCode,MailingStateCode,MailingStreet,Middle_Name__c,MobilePhone,
                                        Mother_Name__c,Nivel_de_Escolaridad__c,Passport_Expiration_Date__c,Passport_Number__c,
                                        Passport_Type__c,Profession_Category__c,Profession__c,Reference_Address__c,
                                        Reference_District__c, References_Municipality__c, References_Province__c,
                                        Reference_Full_Name__c,Reference_Last_Name__c,Reference_Phone1__c,
                                        Reference_Phone2__c,Reference_Second_Name__c,Reference_Second_Surname__c,Relationship__c,
                                        Residence_Address_1__c,Residence_Address_1_City__c,Residence_Address_1_Province__c,
                                        Residence_Address_2__c,Residence_Address_2_City__c,Residence_Address_2_Province__c,
                                        Residence_Year_1_From__c,Residence_Year_1_To__c,Residence_Year_2_From__c,
                                        Residence_Year_2_To__c,Second_Last_Name__c,Skin_Color__c,Special_Characteristics__c,
                                        Title__c,Work_City__c,Work_Country__c,Work_Name__c,Work_Phone__c, Work_Street__c,
                                        Work_State__c, Work_Postal_Code__c,Work_Email__c
									FROM Contact
									WHERE FirstName = :procedure.First_Name__c AND Middle_Name__c = :procedure.Middle_Name__c 
											AND LastName= :procedure.Last_Name__c AND Second_Last_Name__c = :procedure.Second_Last_Name__c
                                             AND Birthdate = :procedure.Birthday__c];

        System.debug('Existe Contacto?: '+ (contacts.size() > 0) );
        return (contacts.size() > 0) ? contacts[0] : null;
      }
      
    /**
    * @description Create new Contact
    * @author Silvia Velazquez | 10-05-2020 
    * @return Contact 
    **/
    public static Contact createContact(Procedure__c procedure){
			return new Contact(FirstName = procedure.First_Name__c, Middle_Name__c = procedure.Middle_Name__c, 
									LastName = procedure.Last_Name__c, Second_Last_Name__c = procedure.Second_Last_Name__c, 
                                    Birthdate = procedure.Birthday__c, Immigration_Status__c = procedure.Immigration_Status__c,
                                    Departure_Date__c = procedure.Departure_Date__c, 
                                    LeadSource = DCExpCheckoutConstants.PROCEDURE_ORIGIN);

			//CRUDEnforce.dmlInsert(new List<Contact>{newContact}, 'DCExpCheckoutContactManager.createContact');
			//return newContact;
    }

    /**
    * @description Create or Update a Contact
    * @author Silvia Velazquez | 10-06-2020 
    * @param Procedure__c procedure 
    * @param Contact c 
    * @return String 
    **/
    public static ContactResultWrapper saveContact(Procedure__c procedure, Contact c){
        c.Birth_Country__c = procedure.Birth_Country__c;
        c.Birth_Municipality_CityT__c = procedure.Birth_Municipality_CityT__c;
        c.Birth_Municipality_City__c = procedure.Birth_Municipality_City__c;
        c.Birth_Province__c = procedure.Birth_Province__c;
        c.Birth_ProvinceT__c = procedure.Birth_ProvinceT__c;
        c.Birthdate = procedure.Birthday__c;
        //c.Category__c = procedure.category;
        c.Departure_Date__c = procedure.Departure_Date__c;
        c.Email = procedure.Email__c;
        c.Eyes_Color__c = procedure.Eyes_Color__c;
        c.Hair_Color__c = procedure.Hair_Color__c;
        c.Father_Name__c = procedure.Father_Name__c;
        c.FirstName = procedure.First_Name__c;
        c.Gender__c = procedure.Gender__c;
        c.Height__c = procedure.Height__c;
        c.Immigration_Status__c = procedure.Immigration_Status__c;
        c.LastName = procedure.Last_Name__c;
        /* c.MailingCity = procedure.city;
        c.MailingCountryCode = procedure.residenceCountry;
        c.MailingPostalCode = procedure.postalCode;
        c.MailingStateCode = procedure.state;
        c.MailingStreet = procedure.street; */
        c.Marital_Status_c__c = procedure.Marital_Status__c;
        c.Middle_Name__c = procedure.Middle_Name__c;
        c.MobilePhone = procedure.Phone__c;
        c.Mother_Name__c = procedure.Mother_Name__c;
        c.Nivel_de_Escolaridad__c = procedure.Nivel_de_Escolaridad__c;
        //c.Passport_Expiration_Date__c = procedure.passportExpirationDate;
        c.Passport_Number__c = procedure.Numero_de_Pasaporte__c;
        //c.Passport_Type__c = procedure.passportType;
        //c.Profession_Category__c = procedure.professionCategory;
        c.Profession__c = procedure.Profession__c;
        c.Reference_Address__c = procedure.Reference_Address__c;
        c.References_Municipality__c = procedure.References_Municipality__c;
        c.References_Province__c = procedure.References_Province__c;
        //c.Reference_District__c = procedure.referenceDistrict;
        c.Reference_Full_Name__c = procedure.Reference_Full_Name__c;
        c.Reference_Phone__c = procedure.Reference_Phone__c;
        /*c.Reference_Last_Name__c = procedure.referenceLastName;
        c.Reference_Phone1__c = procedure.phone1;
        c.Reference_Phone2__c = procedure.phone2;
        c.Reference_Second_Name__c = procedure.referenceSecondName;
        c.Reference_Second_Surname__c = procedure.referenceSecondSurname;*/
        c.Relationship__c = procedure.Relationship__c;
        c.Residence_Address_1__c = procedure.Residence_Address_1__c;
        c.Residence_Address_1_City__c = procedure.Residence_Address_1_City__c;
        c.Residence_Address_1_Province__c = procedure.Residence_Address_1_Province__c;
        c.Residence_Address_2__c = procedure.Residence_Address_2__c;
        c.Residence_Address_2_City__c = procedure.Residence_Address_2_City__c;
        c.Residence_Address_2_Province__c = procedure.Residence_Address_2_Province__c;
        c.Residence_Year_1_From__c = procedure.Residence_Year_1_From__c;
        c.Residence_Year_1_To__c = procedure.Residence_Year_1_To__c;
        c.Residence_Year_2_From__c = procedure.Residence_Year_2_From__c;
        c.Residence_Year_2_To__c = procedure.Residence_Year_2_To__c;
        c.Second_Last_Name__c = procedure.Second_Last_Name__c;
        c.Skin_Color__c = procedure.Skin_Color__c;
        c.Special_Characteristics__c = procedure.Special_Characteristics__c;
        /* c.Title__c = procedure.title;
        c.Work_City__c = procedure.workCity;
        c.Work_Country__c = procedure.workCountry;*/
        c.Work_Name__c = procedure.Work_Name__c;
        /* c.Work_Phone__c = procedure.workPhone;
        c.Work_Street__c = procedure.workStreet;
        c.Work_State__c = procedure.workState;
        c.Work_Postal_Code__c = procedure.workPostalCode;
        c.Work_Email__c = procedure.workEmail; */

        ContactResultWrapper result = new ContactResultWrapper();
        try {
            CRUDEnforce.dmlUpsert(new List<Contact>{c}, 'DCExpCheckoutContactManager.saveContact');
            result.id = c.Id;
            result.msg = 'OK';
            return result;
        } 
        catch (DmlException e) {
            result.msg = 'Error al actualizar el contacto: ' + e.getMessage();
            return result;
        }
    }

    public class ContactResultWrapper{
        public String id;
        public String msg;
    }
}