({
    reset: function(component, helper){
            component.set("v.loading", true);
            component.set("v.showDataTable", false);
            component.set("v.canApplyPayment", true);
            var id = component.get("v.id");
            if (! id){
                var myPageRef = component.get("v.pageReference");
                //alert(JSON.stringify(myPageRef));
                var id = myPageRef.state.c__id;
                component.set("v.id", id);
            }
            helper.initColumns(component, helper);
            helper.getBatchInfo(component, helper); 
            component.set("v.loading", false);
    },
    runServerAction : function(cmp, helper, method, params) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(self, function(response) {
				var state = response.getState();
                if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    },
    
    getBatchInfo: function(component, helper){
        var recordId = component.get("v.id");
        //alert(recordId);
        helper
        .runServerAction(component, helper, "initData", {batchId: recordId})
        .then(function(result) {
      
              //alert(JSON.stringify(result));   
              if (!result) {
                var serverErrorMessage = component.get("v.serverErrorMessage");
                helper.showToast(component, "Error", serverErrorMessage, "error");
              }
     
              component.set("v.loading", false);
              component.set("v.showConfirmation", false);
              component.set("v.pendingAmount", result.amount_Pending_to_Apply);
              component.set("v.cantPendingProcedures", result.procedure_count_to_Apply);
              component.set("v.procedureList", result.batch_items);  
              component.set("v.emptyList", result.batch_items.length == 0);  
              component.set("v.showDataTable", true);
            });
    },
    
    initColumns: function(component, helper){
    	component.set("v.columnFields", [
            {
                  label: "Trámite",
                  fieldName: "procedureNameLink",
                  type: "url",
                  typeAttributes: {
                        label: { fieldName: "procedure_Name" },
                        target: "_self"
                  }      
            },
            {
              label: "Cliente",
              fieldName: "procedure_Contact",
              type: "text"      
            },
              {
              label: "Tipo de Trámite",
              fieldName: "procedure_type",
              type: "text"        
            },
            {
              label: "Fecha de Creación",
              fieldName: "procedure_date",
              type: "date"
            },
            {
              label: "Pago al Consulado",
              fieldName: "procedure_consulate_payment",
              type: "currency",
            },
            {
              label: "Comisión de Agencia",
              fieldName: "procedure_agency_comission",
              type: "currency"
            },
            {
              label: "Pago al Mayorista",
              fieldName: "procedure_distributor_payment",
              type: "currency"
            }           
          ]);
	},
    
    showToast: function(component, title, message, type) {
        
        var toastEvent = $A.get("e.force:showToast");
        //alert(toastEvent);
        if (toastEvent){
            toastEvent.setParams({
                        "title": title,
                        "message": message,
                        "type": type,
                        "duration": 1500,
	                });
    		toastEvent.fire();
        }
        else{
             	var cmp = component.find('notifLib');
                component.find('notifLib').showToast({
                    "variant": type,
                    "header":  title,
                    "message": message
                });
        }
    },
    
    changeSelected: function(component, helper){
         /* update amount selected and proceduresSelected */
        
        var rows = component.get("v.selectedRows"); 
        //alert(JSON.stringify(rows));
        var selectedAmount = 0;
        var cantSelectedProcedures = 0;
        Object.values(rows).forEach(row => {
          	selectedAmount += row.procedure_distributor_payment;
            //alert(row.procedure_distributor_payment);
            cantSelectedProcedures++;
        });
                
        var pendingAmount = component.get("v.pendingAmount");
        var canApplyPayment = !(selectedAmount > 0 && pendingAmount >= selectedAmount);
        //alert(selectedAmount);
        //alert(cantSelectedProcedures);
        //alert(canApplyPayment);
        component.set("v.selectedAmount", selectedAmount); 
        component.set("v.cantSelectedProcedures", cantSelectedProcedures); 
        component.set("v.canApplyPayment", canApplyPayment); 
        
    }
})