/**
 * @description       :
 * @author            : Ibrahim Napoles
 * @group             :
 * @last modified on  : 08-26-2020
 * @last modified by  : Ibrahim Napoles
 * Modifications Log
 * Ver   Date         Author            Modification
 * 1.0   08-21-2020   Ibrahim Napoles   Initial Version
 **/
@isTest
private class CRUDEnforceTest {

    @IsTest
    static void dmlAccessible(){
        Test.startTest();
        Boolean flag = false;

        try {
            CRUDEnforce.dmlAccessible(
                'Account',
                new List<String> {'Id', 'Name'},
                'CRUDEnforceTest.dmlAccessible');
            flag =  true;
        } catch (Exception ex) {
            flag =  false;
        }

        System.assertEquals(flag,
                            Account.getSObjectType().getDescribe().isAccessible(),
                            'Check accesible');
        Test.stopTest();
    }

    @IsTest
    static void dmlInsert(){
        Test.startTest();
        Boolean flag = false;

        try {
            CRUDEnforce.dmlInsert(new List<Account> {new Account(
                                                         Active__c = 'Yes',
                                                         Name = 'District Cuba',
                                                         Phone='2023211169',
                                                         Type='Mayorista',
                										 Sigla__c = 'DCW')},
                                  'CRUDEnforceTest.dmlInsert');
            flag = true;
        } catch (Exception ex) {
            flag = false;
        }
        System.assertEquals(flag,
                            Account.getSObjectType().getDescribe().isCreateable(),
                            'Check accesible');
        Test.stopTest();
    }

    @IsTest
    static void dmlUpdate(){
        Test.startTest();
        Boolean flag = false;
        String message = '';

        List<Account> accountList = new List<Account> {new Account(
                                                           Active__c = 'Yes',
                                                           Name = 'District Cuba',
                                                           Phone='2023211169',
                                                           Type='Mayorista',
            											   Sigla__c = 'DCW')};
        insert accountList;
        accountList[0].Name = accountList[0].Name + ' Test';

        try {
            CRUDEnforce.dmlUpdate(accountList,
                                  'CRUDEnforceTest.dmlUpdate');
            flag = true;
        } catch (Exception ex) {
            flag = false;
            message = ex.getMessage();
        }
        System.assertEquals(flag,
                            Account.getSObjectType().getDescribe().isUpdateable(),
                            message);
        Test.stopTest();
    }

    @IsTest
    static void dmlUpsert(){
        Test.startTest();
        Boolean flag = false;
        String message = '';

        List<Account> accountList = new List<Account> {new Account(
                                                           Active__c = 'Yes',
                                                           Name = 'District Cuba',
                                                           Phone='2023211169',
                                                           Type='Mayorista',
        												   Sigla__c = 'DCW')};
        insert accountList;
        accountList[0].Name = accountList[0].Name + ' Test';

        try {
            CRUDEnforce.dmlUpsert(accountList,
                                  'CRUDEnforceTest.dmlUpsert');
            flag = true;
        } catch (Exception ex) {
            flag = false;
            message = ex.getMessage();
        }
        System.assertEquals(flag,
                            Account.getSObjectType().getDescribe().isCreateable()
                            || Account.getSObjectType().getDescribe().isUpdateable(),
                            message);
        Test.stopTest();
    }

    @IsTest
    static void dmlDelete(){
        Test.startTest();
        Boolean flag = false;

        List<Account> accountList = new List<Account> {new Account(
                                                           Active__c = 'Yes',
                                                           Name = 'District Cuba',
                                                           Phone='2023211169',
                                                           Type='Mayorista',
        												   Sigla__c = 'DCW')};
        insert accountList;

        try {
            CRUDEnforce.dmlDelete(accountList,
                                  'CRUDEnforceTest.dmlDelete');
            flag = true;
        } catch (Exception ex) {
            flag = false;
        }
        System.assertEquals(flag,
                            Account.getSObjectType().getDescribe().isDeletable(),
                            'Check accesible');
        Test.stopTest();
    }

    @IsTest
    static void throwException(){
        Test.startTest();
        SObjectType obj = Account.getSObjectType();

        try {
            CRUDEnforce.throwException('Test.throwException',
                                       CRUDEnforce.CREATE_MESSAGE,
                                       obj);
        } catch (Exception ex) {
            System.assertEquals(false, String.isEmpty(ex.getMessage()), 'Check Exception');
        }

        Test.stopTest();
    }

    @IsTest
    static void throwExceptionField(){
        Test.startTest();
        SObjectType obj = Account.getSObjectType();
        Schema.SObjectField myField = Account.getSObjectType().getDescribe().fields.getMap().get('Name');

        try {
            CRUDEnforce.throwException('Test.throwExceptionField',
                                       CRUDEnforce.CREATE_MESSAGE,
                                       obj,
                                       myField);
        } catch (Exception ex) {
            System.assertEquals(false, String.isEmpty(ex.getMessage()), 'Check Exception');
        }

        Test.stopTest();
    }
}