/**
 * @File Name          : dcPassportReceptionController.cls
 * @Description        : Apex Controller for component dcPassportReception
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 29/7/2020 5:09:37 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/7/2020   Silvia Velazquez     Initial Version
**/
public with sharing class dcPassportReceptionController {

    public static dcSearchContact decodeJson(String searchPattern){
        dcSearchContact objConverted = (dcSearchContact) JSON.deserialize(searchPattern, dcSearchContact.class);
        return objConverted;
    }

    @AuraEnabled
    public static dcSearchContact initData(String searchPattern){
        dcSearchContact searchContact = new dcSearchContact(decodeJson(searchPattern));
        return searchContact;
    }

    @AuraEnabled
    public static dcSearchContactResult count(String searchPattern){
        dcSearchContact searchContact = new dcSearchContact(decodeJson(searchPattern));
        return dcSearchUtility.count(searchContact);
    }

    @AuraEnabled
    public static dcSearchContactResult search(String searchPattern){
        dcSearchContact searchContact = new dcSearchContact(decodeJson(searchPattern));
        return dcSearchUtility.search(searchContact);
    }

    @AuraEnabled
    public static dcSearchContactResult reset(String searchPattern){
        dcSearchContact searchContact = new dcSearchContact(decodeJson(searchPattern));
        return dcSearchUtility.reset(searchContact);
    }
    

    @AuraEnabled
    public static Boolean updateProcedure(String contactId, String procedureId, String passportNumber,Date expirationDate){

        return dcSearchUtility.updateProcedure(contactId, procedureId, passportNumber, expirationDate);
    }
        
}