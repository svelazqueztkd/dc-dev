/**
 * @File Name          : AsyncDcFedexServiceClient.cls
 * @Description        : Class to make Async web service callouts
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 24/7/2020 9:51:41 a. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/7/2020   Silvia Velazquez     Initial Version
**/
//Generated by wsdl2apex

public class AsyncDcFedexServiceClient {
/*     public class ShipmentReplyFuture extends System.WebServiceCalloutFuture {
        public dcFedexServiceClient.ShipmentReply getValue() {
            dcFedexServiceClient.ShipmentReply response = (dcFedexServiceClient.ShipmentReply)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class ProcessTagReplyFuture extends System.WebServiceCalloutFuture {
        public dcFedexServiceClient.ProcessTagReply getValue() {
            dcFedexServiceClient.ProcessTagReply response = (dcFedexServiceClient.ProcessTagReply)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    } */
    public class ProcessShipmentReplyFuture extends System.WebServiceCalloutFuture {
        public dcFedexServiceClient.ProcessShipmentReply getValue() {
            dcFedexServiceClient.ProcessShipmentReply response = (dcFedexServiceClient.ProcessShipmentReply)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class AsyncShipServicePort {
        public String endpoint_x = 'https://wsbeta.fedex.com:443/web-services/ship';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://fedex.com/ws/ship/v25', 'dcFedexServiceClient'};
        /* public AsyncDcFedexServiceClient.ShipmentReplyFuture beginDeleteShipment(System.Continuation continuation,dcFedexServiceClient.WebAuthenticationDetail WebAuthenticationDetail,dcFedexServiceClient.ClientDetail ClientDetail,dcFedexServiceClient.TransactionDetail TransactionDetail,dcFedexServiceClient.VersionId Version,DateTime ShipTimestamp,dcFedexServiceClient.TrackingId TrackingId,String DeletionControl) {
            dcFedexServiceClient.DeleteShipmentRequest request_x = new dcFedexServiceClient.DeleteShipmentRequest();
            request_x.WebAuthenticationDetail = WebAuthenticationDetail;
            request_x.ClientDetail = ClientDetail;
            request_x.TransactionDetail = TransactionDetail;
            request_x.Version = Version;
            request_x.ShipTimestamp = ShipTimestamp;
            request_x.TrackingId = TrackingId;
            request_x.DeletionControl = DeletionControl;
            return (AsyncDcFedexServiceClient.ShipmentReplyFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncDcFedexServiceClient.ShipmentReplyFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://fedex.com/ws/ship/v25/deleteShipment',
              'http://fedex.com/ws/ship/v25',
              'DeleteShipmentRequest',
              'http://fedex.com/ws/ship/v25',
              'ShipmentReply',
              'dcFedexServiceClient.ShipmentReply'}
            );
        }
        public AsyncDcFedexServiceClient.ProcessTagReplyFuture beginProcessTag(System.Continuation continuation,dcFedexServiceClient.WebAuthenticationDetail WebAuthenticationDetail,dcFedexServiceClient.ClientDetail ClientDetail,dcFedexServiceClient.TransactionDetail TransactionDetail,dcFedexServiceClient.VersionId Version,dcFedexServiceClient.RequestedShipment RequestedShipment) {
            dcFedexServiceClient.ProcessTagRequest request_x = new dcFedexServiceClient.ProcessTagRequest();
            request_x.WebAuthenticationDetail = WebAuthenticationDetail;
            request_x.ClientDetail = ClientDetail;
            request_x.TransactionDetail = TransactionDetail;
            request_x.Version = Version;
            request_x.RequestedShipment = RequestedShipment;
            return (AsyncDcFedexServiceClient.ProcessTagReplyFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncDcFedexServiceClient.ProcessTagReplyFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://fedex.com/ws/ship/v25/processTag',
              'http://fedex.com/ws/ship/v25',
              'ProcessTagRequest',
              'http://fedex.com/ws/ship/v25',
              'ProcessTagReply',
              'dcFedexServiceClient.ProcessTagReply'}
            );
        }
        public AsyncDcFedexServiceClient.ShipmentReplyFuture beginDeleteTag(System.Continuation continuation,dcFedexServiceClient.WebAuthenticationDetail WebAuthenticationDetail,dcFedexServiceClient.ClientDetail ClientDetail,dcFedexServiceClient.TransactionDetail TransactionDetail,dcFedexServiceClient.VersionId Version,String DispatchLocationId,Date DispatchDate,dcFedexServiceClient.Payment Payment,String ConfirmationNumber) {
            dcFedexServiceClient.DeleteTagRequest request_x = new dcFedexServiceClient.DeleteTagRequest();
            request_x.WebAuthenticationDetail = WebAuthenticationDetail;
            request_x.ClientDetail = ClientDetail;
            request_x.TransactionDetail = TransactionDetail;
            request_x.Version = Version;
            request_x.DispatchLocationId = DispatchLocationId;
            request_x.DispatchDate = DispatchDate;
            request_x.Payment = Payment;
            request_x.ConfirmationNumber = ConfirmationNumber;
            return (AsyncDcFedexServiceClient.ShipmentReplyFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncDcFedexServiceClient.ShipmentReplyFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://fedex.com/ws/ship/v25/deleteTag',
              'http://fedex.com/ws/ship/v25',
              'DeleteTagRequest',
              'http://fedex.com/ws/ship/v25',
              'ShipmentReply',
              'dcFedexServiceClient.ShipmentReply'}
            );
        } */
        public AsyncDcFedexServiceClient.ProcessShipmentReplyFuture beginProcessShipment(System.Continuation continuation,dcFedexServiceClient.WebAuthenticationDetail WebAuthenticationDetail,dcFedexServiceClient.ClientDetail ClientDetail,dcFedexServiceClient.TransactionDetail TransactionDetail,dcFedexServiceClient.VersionId Version,dcFedexServiceClient.RequestedShipment RequestedShipment) {
            dcFedexServiceClient.ProcessShipmentRequest request_x = new dcFedexServiceClient.ProcessShipmentRequest();
            request_x.WebAuthenticationDetail = WebAuthenticationDetail;
            request_x.ClientDetail = ClientDetail;
            request_x.TransactionDetail = TransactionDetail;
            request_x.Version = Version;
            request_x.RequestedShipment = RequestedShipment;
            return (AsyncDcFedexServiceClient.ProcessShipmentReplyFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncDcFedexServiceClient.ProcessShipmentReplyFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://fedex.com/ws/ship/v25/processShipment',
              'http://fedex.com/ws/ship/v25',
              'ProcessShipmentRequest',
              'http://fedex.com/ws/ship/v25',
              'ProcessShipmentReply',
              'dcFedexServiceClient.ProcessShipmentReply'}
            );
        }
        /* public AsyncDcFedexServiceClient.ShipmentReplyFuture beginValidateShipment(System.Continuation continuation,dcFedexServiceClient.WebAuthenticationDetail WebAuthenticationDetail,dcFedexServiceClient.ClientDetail ClientDetail,dcFedexServiceClient.TransactionDetail TransactionDetail,dcFedexServiceClient.VersionId Version,dcFedexServiceClient.RequestedShipment RequestedShipment) {
            dcFedexServiceClient.ValidateShipmentRequest request_x = new dcFedexServiceClient.ValidateShipmentRequest();
            request_x.WebAuthenticationDetail = WebAuthenticationDetail;
            request_x.ClientDetail = ClientDetail;
            request_x.TransactionDetail = TransactionDetail;
            request_x.Version = Version;
            request_x.RequestedShipment = RequestedShipment;
            return (AsyncDcFedexServiceClient.ShipmentReplyFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncDcFedexServiceClient.ShipmentReplyFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://fedex.com/ws/ship/v25/validateShipment',
              'http://fedex.com/ws/ship/v25',
              'ValidateShipmentRequest',
              'http://fedex.com/ws/ship/v25',
              'ShipmentReply',
              'dcFedexServiceClient.ShipmentReply'}
            );
        } */
    }
}