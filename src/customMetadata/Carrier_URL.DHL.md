<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DHL</label>
    <protected>false</protected>
    <values>
        <field>Carrier__c</field>
        <value xsi:type="xsd:string">DHL</value>
    </values>
    <values>
        <field>URL_Address__c</field>
        <value xsi:type="xsd:string">https://www.dhl.com/en/express/tracking.html?AWB</value>
    </values>
</CustomMetadata>
