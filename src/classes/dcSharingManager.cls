/**
 * @File Name          : dcSharingManager.cls
 * @Description        : Class to manage the creation of dynamic sharings.
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 * Ver       	Date           	Author      		    Modification
 * 1.0   		10-19-2020		Silvia Velazquez		Initial class definition	
**/
public class dcSharingManager {
    
    public static void shareRecords(List<sObject> records, String sharingTypeName, String accessLevel, String userOrGroupId, String rowCause){
		        
        List<sObject> sharingsList		= new List<sObject>();
        sObject sharing					= null;
        Schema.SObjectType sharingType  = Schema.getGlobalDescribe().get(sharingTypeName);
        
        if (sharingType == null) {
            DCException.throwPermiException('dcSharingManager.shareRecords');
        }
        
         if(!(sharingType.getDescribe().isAccessible() &&
             sharingType.getDescribe().fields.getMap().get('Id').getDescribe().isAccessible() &&
             sharingType.getDescribe().fields.getMap().get('ParentId').getDescribe().isAccessible() &&
             sharingType.getDescribe().fields.getMap().get('AccessLevel').getDescribe().isAccessible() &&
             sharingType.getDescribe().fields.getMap().get('RowCause').getDescribe().isAccessible() && 
             sharingType.getDescribe().fields.getMap().get('UserOrGroupId').getDescribe().isAccessible()
             )) {
            DCException.throwPermiException('dcSharingManager.shareRecords');
        }
        
        // Prepare sharing records
        for(sObject record : records){
			sharing  = sharingType.newSObject();
            sharing.put('ParentId', record.Id);
			sharing.put('AccessLevel', accessLevel);
			sharing.put('RowCause', rowCause);
            sharing.put('UserOrGroupId', userOrGroupId);
            sharingsList.add(sharing);
        }
        
        if(sharingsList.size()> 0){
            insert sharingsList;
        }
	}

    
    /*
     * Sample Code
     * 
     *  Dummy__c d  = new Dummy__c();
        d.Name		= 'Test ApexSharing';
        insert d;
        
        List<Dummy__c> records = [select id from dummy__c];
        dcSharingManager.shareRecords(records, 'Dummy__Share' , 'Edit', '00G4W000004xDAB', 'Dynamic_Apex__c');

	*/
}