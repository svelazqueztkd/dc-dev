/**
 * @File Name          : dcContactProcedureSearchTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcContactProcedureSearchTest {

    @isTest
    static void test(){
        Test.startTest();
        dcContactProcedureSearch cps = new dcContactProcedureSearch();
        Test.stopTest();
        System.assertNotEquals(null, cps, '');
    }
}